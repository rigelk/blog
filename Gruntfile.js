module.exports = function(grunt) {
  grunt.initConfig({
    // Remove unused CSS across multiple files, compressing the final output
    uncss: {
      dist: {
        files: [
          { src: 'public/**/*.html', dest: 'public/screen.css'}
        ]
      },
      options: {
        compress:true
      }
    }
  });
  // Load the plugins
  grunt.loadNpmTasks('grunt-uncss');
  // Default tasks.
  grunt.registerTask('default', ['uncss']);
};
