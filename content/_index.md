+++
paginate_by = 8
sort_by = "date"
page_template = "page.html"
insert_anchor_links = "right"
+++