+++
title = 'Contact'

[extra]
no_header = true
+++

<div class="flex justify-between">
  <span>I am rigelk.</span>
  <img
    src="https://static.drycat.fr/mastodon/accounts/avatars/000/007/134/original/24a5bcf61ec55bed.png"
    class="br2 h4 w4 dib" alt="avatar" style="margin-right: 0 !important;" >
</div>

You can contact me on <i class="fa fa-mastodon" aria-hidden="true"></i> [Mastodon](https://miaou.drycat.fr/@rigelk),
or via `echo c2VuZG1lbWFpbEByaWdlbGsuZXUK | base64 -d`, possibly with my GPG key[^1]. You are expected to follow netiquette in subsequent exchanges.

[^1]:
```none

-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFmhrj4BEADE223C5UH4r6fETG6AuNXMQkpvnTZSMLrM0Ye/RxMj+huEQgad
aXigWW8nUyiNx/8iZzoNkrxjO7CG8SK/hPNbKDt6qxOqJu7tLGTdRZ8Nxg6Cbz4b
jFg1mCllwYftZRTpHBPtbMudG+Fz51xfHcaA3oYgczFd/VLKEtmcxEUt72I7fRl/
r1pYAV/yGUP7RrV2Uv0pLvcvIa3OXuVeQ4A2ws66pUASo/OVg6HTSpI8TgM+i4oC
MWx+ZcqHw1VoBvvp23f6qIr9/HNiL/OyYQ4lUS+/bD1SdCyGOnaiiZSSDlicGEs1
Yb4DWU9FTi9C4fTlOlW09olfawLcgvLUv25WN158j2+T6YnffovteAGhFd9gCQVA
6P0p2yy5GLsUT+NFcQDaB0lYfLT07R3BGAJJvtBqKbqq74IafaoMxChQ6rCiyCCx
aXcnHJk01qDvjDNreQZH99stZkLja96W0DuFR11tuDR7FEHvVrfqLjEs9oEk5QVP
wRC2CMhzq5HvgHdtZEa3VGcqlXiRq6l8He5ry+7KHCnDU5ljdWsyOZTfrHV4zJSy
NviEIZ8qOA/xF04fQMr47NcwALqTi8bR9pOof3Q+WvP9KMynFWVrocXF/JjBC/NT
rp8HmDCp9tUBVgwZ9xxVsYecWwhtPz5hm0Myevyc6T4mjawbgPeOYERdPwARAQAB
tCFSaWdlbCBLZW50IDxzZW5kbWVtYWlsQHJpZ2Vsay5ldT6JAlQEEwEIAD4WIQTF
lBu/XKy3/GN6/qbqEpcbDkOPNgUCWaGuPgIbAwUJAeEzgAULCQgHAgYVCAkKCwIE
FgIDAQIeAQIXgAAKCRDqEpcbDkOPNumJD/9Ab56mTMHZpg2xR2vEwQo4fB1/FpFQ
W+j+qwfTQ2YEhUVGav+MAqvq0NVcb7cdSVuwVV+6puBoAEKjrvNz4jFDai3koXTo
6/8t5H7z37ZDUHPA7e1T3ONGDUhuw6+81RjPplmLXlH5/mNDWSxF+WbjDsHNC2aw
XC1aT5Arp7VbheydrPS4hZUiAKmVQBvF7Fz7uVLNYCBDlZ50b9s2Z17cnlLVIhxV
74YS6Oo5yq5yuNPoT7KRFiLrAmxxq0CEdsDmr5EEb5BR0SUI6zjlBZTZ2CoXo9bK
XJuf37jZM2kcgEMP/wD87ErMQ/iVS8OXEZlxMoxlWIoxXsSpImcDJ1NwLyRPfyBX
MP900ziduXNaORQZl4WtIA2oFOiG67wLyek27bwRkJOTsQK8P3Etdgl3X3QXutLQ
s2iJgG3in88AHE1CNcP8h52xN23/Bp3f0UYIJxnI4rtbNh2s13mXh7Axnfjbo4Nx
nb8/2FjBGTrCjBsto/ns/BfQbTc/9l/XgQGReBmujenI/CKjYvjQ48pW45kxdfnu
lAqa0ND81WTLKZ5kjyQ3Fw8tHICD3erHNst+f6/YmQOzKkHoVFrV3FaogKTAmKYZ
9hl9uOBpFwqDhKHN2X8G+eL1AM4vh0MAx7IUsziJ09eu70lrALkBAfY5T8uDTj0s
C8YtW1JbIV1TMA==
=aleA
-----END PGP PUBLIC KEY BLOCK-----
```
