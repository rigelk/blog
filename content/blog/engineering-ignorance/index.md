+++
title = 'The age of engineering ignorance'
description = "A note written out of exasperation for the state of gender parity in IT. The issues might be more deeply rooted in society, but we have to act within the IT sector too."
date = 2016-07-30
+++

Highschool students may admit it or not, they ignore a lot about the choices they make. Taking whatever course we want? Parents often let us go our way. And when they push us, it’s just not our choice. Duh.

Most students haven’t experienced enough to actually choose a major. They often mimic others or just choose what seems to be the easiest way out. I am of the few who made their mind about what they wanted to do before graduation, so that i could choose a corresponding major for my last year in highschool. That didn’t make my life easier, but at least I am sure of what I want to do. A lot of people don’t have that chance and choose either:

- the easiest major
- the same major as friends
- the major they see fit their gender/lifestyle

This is a common situation for students. It’s easy to listen to these voices in our heads – invisible scripts – that dictate our everyday behavior and impact our lifelong success. They may feel that is “enough” if the subject feels “right”. They may even avoid trying to avoid failing.

## Schools: should we care?

As long as the default choices are in average balanced both in parity and quantity, that ignorance only has two (though significant) consequences:

- feeling low
- postponing career start/progression[^1]

[^1]: once employed, it is way more difficult to find time/money to study again and change a career path.

But as one witnesses in college, parity is rarely respected and quantity – though difficult to consider beyond capacity – no better. That means default choices are replicating sexist quotas.

Unfortunately schools don’t have the budget to change the situation and they’re not the victims of that lack of information given to students. We are. Engineering jobs are. Men/Women ratios in all professions are.

That is a dangerous mix. Students won’t be as happy as they could be, we won’t get a good parity, and we will have our lot of lost souls, that just aren’t doing the right job. Yes, that means companies could find more easily engineers! With people choosing a job rather than a default one, no wonder they prove more valuable to companies.

## Beyond school: the IT example

As a computer scientist, I often see the results of both ignorance and history facts: the IT sector had a better women ratio in the fifties than it has now (see the ENIAC girls pictured in this article), as the coding task was only devoluted to women. Coding in its early days consisted indeed mainly in reproducing branching sequences on the target machine – that arid work was unattractive to men, hence the high ratio women/men in the field at the time.

Then what made the IT field get that low ratio we have now? What shaped the profession and how non-programmers and potential aspiring programmers perceive it? And how might all that be connected to our ongoing struggle to achieve more diversity in the industry?

In her speech a programmer is… (2015, Codemotion Berlin), Brigitta Böckeler pointed out that back in the 60s, programming was considered a “Black Art” and that “Programmers are born, not made”. So… Forty years later, we still hear these stories:

> There are people who are born to do this, and I am not one of them. And it’s definitely not one of those things that, like, “Oh, with practice, you will become one who is born to do it.” …
> You just gotta be born to be like, ‘Computers! Yeah! They are awesome!! They are my life!’ You know, a lot of computer scientists, that’s all they do.”
> <cite>a CS student, ca. 2014</cite>

That couldn’t be worse: even us programmers consider ourselves to be part of a community that corresponds to the image we need to get rid of to reach balance. The fact is, that image finds its roots with the recruiting tests to find programmers in the 60s. At the time, companies didn’t know what were the common caracteristics among programmers. So they looked at the programmers they had at the time. They found that they were:

- crazy about puzzles
- tend to like research applications and risk-taking
- dislike routine and regimentation
- and don’t like people…

(source: William M. Cannon and Dallis K. Perry. 1966. A vocational interest scale for computer programmers. In Proceedings of the fourth SIGCPR conference on Computer personnel research (SIGCPR ’66), A. W. Stalnaker (Ed.). ACM, New York, NY, USA, 61-82. )

> “If we are using a single model to identify potential programmers, we
> will miss many potential students.”

So the first thing is to fight that model we still convey fifty years later, by not using it to recruit and developing models we would like to positively shape the IT field.

## Create experiences

And not just conferences.

You code? Go volunteer to teach practical IT at your local highschool, be it for a workshop or in the long run.

You’re a techie girl? Go show girls and boys computer science is not (only) for hooded nerds.

You’re a teacher? What are you waiting for? Make your students do something with practical purpose and a clear vision that they can later leverage not only as a grade, but as an insightful experience. All students are skeptical of the course they are given. Even the most theoretical course can be interesting if they focus (and of course they don't), but focus is not a magical thing that some students have and others don't. Focus comes with interest, and interest with a sense of purpose, so make the latter very clear upfront.
