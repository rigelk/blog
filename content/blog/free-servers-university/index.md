+++
title = 'Free servers for students of the university'
description = "Gathering servers from a local company, barely aged, for fun and profit at the local CS association."
date = 2016-09-09
[taxonomies]
tags = ["linux", "network"]
categories = ["test"]
+++

Sometimes it becomes tricky to get a good machine to run tests or research programs, be it as a Ph.d student or as freshman willing to discover what bare metal means.

As I’ve always been interested in hardware, I couldn’t feed my appetite for network programming and machines with the few courses provided by the university. Seems like those courses only teach you to write down pseudo-code on paper…

That’s why I joined the IT association ASCIII : to get my hands dirty ! The association, however, was empty at first as I only found a Dell Poweredge 4200. An old 2003 server aimed at small offices for backup. It was more like a bench than a computer (for those wondering it did end up as a bench ;). Pwah ! That is certainly the worst thing I could find.

So I decided to gather some hardware to play with.

## A teaching platform

I come from the Internet. A place where one needs to learn by oneself. It is quite different however down here on planet Earth : one needs tools to experiment with and show others.

That comes with physical machines. The kind of you can play with, look at the internals and make work as long as you care about them. Well, I could have risen an army of old computers but that wouldn’t fit in the small office of the [ASCII](http://ascii.univ-nantes.fr/) ! Plus, servers are a different kind of hardware, more specific but also more reliable. They bring a whole new dimension to hardware understanding and their firepower shadows their age. (with an average of 8 years)

## A testlab

With great power come great responsabilty. Well, not here. Those machines are here to get dirty and anybody can test what he wants.

We ended up with no less that 5 running servers by the end of the 2015 academic year :

- 3 HP Proliant 3650 (8GB RAM/140GB HDD each)
- 1 Dell PowerEdge 1750 (4GB RAM/140HB HDD)
- 1 homemade server with 16GB RAM and ~300GB mixed SATA/PATA drives

## Moar service ideas

The number of servers a bay can hold is more than enough to get a part of it for the testlab, and the other for durable services.

At least, those services are reachable in the ASCII network. It is quite small but with a few Wifi antennas it can cover the whole campus !

As for the services involved, it can be as cool as a showcase of students’ work, an archive to examination corrections, or it can be as fun as a gaming server, for furious in-university gaming (which already exists but lacks good infrastructure). It can and will eventually more be research-oriented, with the disposal of servers to doctorate students for heavy calculus.
why calculus ?

Well, after SETI@HOME and such decentralised projects which give you the ability to dispose your machine to others when it’s not used by you (more specificaly, it’s giving NASA CPU cycles you don’t make use of), the idea of letting the cluster of servers I had gathered in an idle state (due to its use by university students only being made during work hours) seemed unthinkable. That would have been a huge waste.

First, a waste of electricity as the machines are conveniently running 24/7. Then, a waste of hardware as servers wear slowly.

Then, I guess the choice is due to our commitment to the libre/open source community. When you get so many it’s always good to give back, be it with pure calculus or remote compilation for open source projects.