+++
title = 'Reverse-engineering Android apps to regain freedom'
description = "Basic app reverse-engineering to understand how to practice app cleaning. Not scalable but insightful."
date = 2017-06-08
[taxonomies]
tags = ["android", "linux", "security"]
categories = ["tutorial"]
+++

Hot Damn. Mobile applications nowadays make me thinks of stone age as far as freedoms are concerned. My latest example was with Le Monde’s La Matinale, which displays two fullpage video ads every time you launch the app or refocus on it. Let’s try to change that behaviour.

To reverse-engineer La Matinale du Monde, I gathered information by exploring its code. It apparently uses [MRAID (Mobile Rich-media Ad Interface Definitions)](http://www.iab.com/wp-content/uploads/2015/08/IAB_MRAID_v2_FINAL.pdf) through [Smart Adserver](http://smartadserver.com/publisher-platform/premium-programmatic/)’s service and A4S’s library. It might seem useless to do such background checks, but that case proved useful as Le Monde publishes [its explanation](http://mpublicite.fr/) (for marketing purposes) of the terms used to describe parts of the application: Topsitial, Intersitial.

![](topstitial.png)

Our application makes use of Topsitial advertising, as seen by its rather extended use in the extracted code (through dex2jar). Chosing a modification in its code was rather simple: in the configuration handler, return null on every Topsitial instanciation. I used **apkstudio** to do just that:

![](dlavik.png)

## Part 2: compiling, packaging, signing

Compiling is really simple through apkstudio. Maybe a simple setting of the RAM usage away and you’re done. Then, signing requires you to create a key to sign with. First, generate a keystore using 

```bash
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000
```

Then generate the key using **apk-signer**:

![](signer2.png)

The alignement is an optional procedure to do before using your app in production:

![](signer.png)

And _Tada!_, your app is ready, with no ads! The resulting app is available on Aptoid, for study purposes and no warranty, under rigelk’s store.