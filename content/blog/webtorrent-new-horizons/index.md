+++
title = "WebTorrent, le bon, le brut, l'abandonné"
description = "7 ans après, quel bilan pour WebTorrent ? Quelles technologies bloquent son usage et quelles alternatives méritent notre attention ?"
date = 2020-04-25
[taxonomies]
tags = ["web", "network", "p2p"]
categories = ["opinion"]
+++

Comment vous parler du web décentralisé en 2020 ? La décennie passée a été riche en petites innovations qui ont changé la donne pour autant de petits acteurs. On a parlé de web 3.0, de DWeb, d'économie distribuée ; j'y reviendrai. Celle-ci venait prendre la relève de celle de la décennie passée et son partage frénétique de fichiers, elle-même s'inspirant de celle passée alors où les espoirs de décentralisation d'un Internet naissant venaient s'échouer sur des coûts que la simplicité seule de la centralisation ne suffisait plus à financer. Restait alors un vide, que les idéaux et les usages mettront encore longtemps avant de s'élargir à tous les pans de la société.

Mais pourquoi cette quête de décentralisation ? Certains s'offusquent que l'on critique la centralisation de leur solution technologique, et préfèrent nous enjoindre de ne regarder que la carotte qu'ils nous tendent, ce service qu'ils « offrent ». Personne n'est dupe ; on dit même que « si c'est gratuit, c'est vous le produit », non ? La sagesse collective est tétanisée devant la technologie, et oublie le principe de précaution qu'elle appliquerait autrement : ne pas faire aveuglément confiance aux inconnus, ne pas se mettre à nu en public, etc. Car oui, sur Internet, tout est affaire de confiance aveugle. Vous ne savez pas qui fait tourner le serveur, qui fait le profit pour le payer. Et du « client », le navigateur est bien plus souvent le bétail que l'on tond sur l'autel de la monétisation de la vie privée.

Le web distribué veut rebattre les cartes. Pas de serveur central, et donc pas de besoin d'en payer, tout simplement. Si serveur il y a, il est de petite échelle pour que les coûts restent absorbables par un individu, et n'est certainement pas le seul serveur fournissant le service.

### Héritier de BitTorrent {#heritier}

Si les préoccupations précédentes sont aujourd'hui au cœur des communautés libertaires d'Internet, les années 2000 voyaient ces technologies sous un angle plus utilitaire, cherchant à répondre à la massification des usages et plus particulièrement à la diffusion de fichiers, dont le transfert représentait un défi important : sur un serveur unique, plus un fichier est demandé, moins il est accessible ; si chaque client devient serveur après avoir fini de télécharger le fichier, la tendance précédente est inversée et la qualité de service améliorée.

BitTorrent partit de ce constat et proposa dès 2002 un logiciel qui deux décennies plus tard reste le standard de partage de fichiers distribué. Il a été conçu pour limiter l'usage d'un serveur à sa plus simple expression (un tracker ou un nœud DHT), et si la découverte de contenu reste un problème qu'il ne résout pas, il a permis à des communautés de partage de naître et prospérer. S'affranchissant d'un censeur, il peut ignorer les contrôles couramment placés sur les plateformes centralisées où le contenu doit respecter le droit d'auteur.

BitTorrent est cependant une technologie qui n'a pas suivi la migration des usages d'Internet vers le web, et WebTorrent vient changer cela. Il permet de faire de chaque navigateur un client BitTorrent en puissance. Ce serait probablement passé inaperçu en la présence préalable d'un protocole de diffusion de fichier efficace de navigateur à navigateur, mais aucun n'existait alors qui soit réellement efficace en 2013. Les usages sont alors multiples, permettant de partager des vidéos sans avoir à installer un client dédié, et de pouvoir utiliser un simple navigateur quelle que soit la platforme : ordinateur, tablette, portable, télévision, voiture (sic), etc.

Un des objectifs de WebTorrent est de maintenir la compatibilité avec le *protocole* BitTorrent, mais pas forcément avec les *transports* traditionnellement utilisés par ses clients. De fait, WebTorrent utilise WebRTC comme transport, là où les clients classiques utilisent TCP, μTCP ou UDP.

{{ image(path="webtrump", webp=true, png=true, style="min-width: 300px; width: 30vw", sensitive=true) }}

En résulte un double-réseau de pairs qui ne peuvent communiquer entre eux à moins de supporter un protocole de chaque groupe, ce que les navigateurs ne permettent pas. En effet WebTorrent est tributaire des APIs des navigateurs - c'est un problème que ces derniers tendent à vouloir résoudre par l'installation d'extensions, même si leur API d'extension ne permet pas encore autant de fonctionalités.

Le fait que WebTorrent utilise le protocole BitTorrent circonscrit aussi ses applications à du téléchargement ou du *streaming simple*, quand l'intérêt principal de WebTorrent est de permettre l'usage et la diffusion concomitantes de ressources : la technique de diffusion limite largement certains usages, plus dynamiques, qui se sont répandus récemment pour répondre à des cas d'usages particuliers : streaming *en direct*, accès aléatoire ou partiel. D'autres limitations sont propres à l'implémentation de WebTorrent, comme sa capacité à limiter le débit ou le partage en cas d'usage mobile.

### Tributaire de WebRTC et son écosystème {#tributaire-de-webrtc}

Si WebTorrent a eu un tel succès, c'est grâce à une technologie sous-jacente, WebRTC, et son support dans la plupart des navigateurs. WebRTC est la « colle » qui permet la connection entre navigateurs quand HTTP ne permet historiquement aux navigateurs que les connections sortantes, vers un serveur. WebRTC permet aux navigateurs de se décentraliser. Mais WebRTC est complexe, trop complexe pour certains, et ses implémentations hors d'un navigateurs sont rares.

Autrement dit, les clients BitTorrent classiques n'ont toujours pas ajouté de support pour WebRTC et donc WebTorrent 7 ans plus tard. Pire, à cause de cette dépendence à WebRTC les utilitaires et bibilothèques dépendent pour la plupart d'un navigateur pour utiliser WebTorrent, limitant grandement leur nombre et les languages avec lesquel·le·s les utiliser.

En comparaison, Dat comme IPFS ont fait un bien meilleur travail (mais aussi bien mieux financé) de fond avec deux implémentations de référence : une côté navigateur, une côté côté système. De fait, leur écosystème pourtant naissant est riche d'intégrations dans divers languages. Cela n'en fait pas un meilleur candidat pour autant, IPFS n'étant pas épargné par des problèmes similaires de performance et d'adéquation pour le streaming.

Une mention honorable revient à [rawrtc](https://github.com/rawrtc/rawrtc), qui a lutté pour intégrer WebRTC dans plusieurs clients BitTorrent de bureau avec une implémentation native indépendante de celle de Chrome.

### Pourquoi parler de streaming ? {#pourquoi-streaming}

Je ne suis pas neutre, je participe au dévelopement d'une application de streaming : [PeerTube](https://joinpeertube.org). Tous les problèmes évoqués, nous les avons rencontrés, et plus encore. Quand il s'agit de streaming différé (pas un *live*), la vidéo est stockée sous forme d'un fichier unique avec BitTorrent. Et lorsqu'un client charge cette vidéo, il est courant de vouloir aller à un timecode précis, voire de sauter à différentes parties de la vidéo. Tout cela suppose de savoir à quels blocs de la vidéo correspondent un timestamp donné, et cela suppose d'avoir connaissance d'une métadonnée stockée au début du fichier, [l'en-tête MOOV](https://www.adobe.com/devnet/video/articles/mp4_movie_atom.html). Avec BitTorrent, l'en-tête ne sera pas plus téléchargé en priorité qu'une autre *chunk* du fichier.

Certains clients BitTorrent facilient ce téléchargement en forçant la priorité sur les premiers *chunks*, mais WebTorrent ne l'expose pas, et les bibliothèques[^1] permettant de lire un stream video depuis WebTorrent ne sont pas plus riches non plus. Le problème de richesse de l'écosystème autour de WebTorrent se rappelle douloureusement à nous dans ce cas.

Cela ne paraît pas grand chose, mais c'est cet en-tête qui permet de faire la correspondance entre un bloc de la vidéo et un timestamp. Sans cela, on est condamné à faire une approximation de la position du chunk dans le *stream*. C'est en général innefficace et cela se voit, en particulier sur les longues vidéos, et les petites vidéos où le timestamp est arrondi.

[^1]: [https://github.com/webtorrent/webtorrent/issues/1097](https://github.com/webtorrent/webtorrent/issues/1097)

### Quel espoir pour WebTorrent ?

Avec deux boulets au pied mais aussi deux technologies majeures et couramment utilisées, WebTorrent n'est pas prêt de quitter le paysage du web dans un futur proche. Mais des questions se posent quant à son évolution, étant donné que son dévelopeur principal n'est plus actif (à se disperser avec près de 200 paquets sur npm, on peut comprendre…) ; les co-mainteneurs n'assurant plus qu'une maintenance de réparation.

Quoiqu'il en soit, son objectif de compatibilité avec BitTorrent est à la fois un tremplin pour sa popularité et un blocage certain pour la compatibilité avec des applications temps-réel, où l'immutabilité de ses données *metainfo* l'empêche de couvrir l'ajout dynamique de *chunks* lors d'un *live*.

WebRTC est moins problématique sur le long terme alors que de nouvelles implémentations hors du navigateurs émergeront - gageons alors que cet écosystème profitera à WebTorrent.
