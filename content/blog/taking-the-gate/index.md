+++
title = 'Taking the gate'
description = 'Essay about networks and their gatekeepers: our routers, small home devices that could do so much more to build a positive future for the Internet.'
date = 2018-04-06
[taxonomies]
tags = ["network"]
categories = ["opinion"]
[extra]
glossary = [
  {word = "CHATONS", def = "Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires. A french initiative of Framasoft aiming at the creation of hosting/service providers that work in the open as associations or collectives, ensure a neutral access to services and guarantee a service respectful of your data. A alternative to business model-biased centralized services."}
]
+++

Taking the gate is no small feat. It's the drawbridge to the fort. It's the symbol of power and the first thing the traveler sees from the outside. It's the separation between savage and civilized worlds. Unfortunately the portcullis are closed to us.

{{ image(path="draw_bridge", webp=true, png=true) }}

_engraving of a medieval drawbridge, William & Robert Chambers Encyclopaedia - A Dictionary of Universal Knowledge for the People (Philadelphia, PA: J. B. Lippincott & Co., 1881)_

Fast forward to our times, the gateway to our homes has long been left out of sight. We don't really know what to do with it and for better or worse it *just works*™. It's often a simple enough device that just does routing and modem tasks. Period. Most people plug it and forget about its existence until a guests needs wireless access, only to discover that *ah, a hex string is complicated. What?! I could change it?*

Yet it's certainly one of the most valueable piece of hardware you might have. It is the public-facing device of your home, and it certainly also protects it with NAT and firewall. All of that, transparently. Wow. If it were not for this protection, we would all need to learn how to defend ourself against attackers that could directly access our machines from the Internet.

It's also the last nail in the coffin of the nineteen-seventies, when the Internet was a small decentralized collective of computers. Computers could communicate directly with each other to communicate, exchange small services, and there were no gates. It was arguably another era, where security was not much the same concern. Then came back the gates. Users wanted the security of the gate, and it certainly did protect them. They could see the web but the web could not see them. The old Internet lost something back then, since all machines could not be reached anymore. Some of them remained, open trading posts - public servers -, providing goods and services to the travelers adventuring past their gates.

Now users are left with gates no one hardly ever slides up, and trading posts spying on them. There are safe heavens. Servers and services that guarantee your privacy. There are also inns and houses you can rent to stay private, but it comes at a cost.

## The lie of self-hosting {#self-hosting}

Be it on servers you rent from hosting providers or on a local machine, there are many solutions to self-host. But in the end you always need a machine to stay online most of the time, with enough storage and modern services. It can become tricky to setup, as seemingly low entry barriers are already beyond the few clicks required to open an account in a centralized service. In the end, we have to agree there will never be a single way to self-host.

But is self-hosting any good? The question deserves an answer. Yes, it provides the best alternative in terms of data privacy and modularity. You can litterally install any service you want as long as you have the code and the time to do the installation and maintenance. But these are hard tasks. Even with the help of modern turnkey solutions, your machines can suffer from outage and hardware failure, and then you must know how to restore backups, if any.

Really self-hosting isn't easy. Technology isn't easy. Let that sink in. We have hidden the complexity of technology behind shiny flat design interfaces, and suprisingly, it worked. User interfaces decide which service will win the heart of users, independently of its features or respect of the user. It's quite logical when you don't understand how the service works. But nice interfaces are something lacking in the free software world. Worse, their integration in the overall user experience is still a second class citizen. That subject alone deserves its own dedicated post.

All in all, self-hosted alternatives have not failed us but the users that need it most: the rest of the population, still gated and taken hostage at trading posts. We need to give them more possibilities, and removing the gates is certainly an interesting one. Not an easy one, but we'll come to that later. But even as hard as it can be, it relieves of an obstacle that has forced us to design convoluted programs to circumvent them, making programs that ultimately are out of reach of the average user.

## Not in the fort nor on the road {#fort-or-road}

By now it should be obvious that if we can't really solve the question of *how* that simply, we could help solve the *where*. Plenty of solutions exist to set up a decent software suite and even deal with automated backups. It won't have everything and I certainly don't recommend to put all you services on it, because you know, *mail is hard*. Anyway, that leaves us a good margin to do an already reasonable bunch of services on our machine.

That is, if we have a machine.

The average user has their laptop or desktop computer. Or both. But they certainly don't want to dedicate it to a NAS purpose. That's "wasting a computer", right? And renting one at a hosting provider is money, so it's something out of reach, right?

That's where the router becomes a nice target. It's litterally the only device that everyone has running night and day. It's a low-end, low-power device. Oh yes, you can't put a lot of services on it, but its potential to regain privacy is no less interesting, since by default all devices that connect to it will benefit from settings such as DNS, ad blocking or VPN features. A DNS blackhole for ads? done. A DNS server that forwards requests over TLS? easy, the setup is done once for every device on the LAN. A VPN linking together your LAN with those of your parents/friends? check.

These network services, provided LAN-wide, are more interesting that what even your average NAS provides. It allows to bind networks together, provide home automation control, intrusion detection and properly configured, it can [tame spying devices](https://pi-hole.net/2017/02/22/what-really-happens-on-your-network-find-out-with-pi-hole/).

We need to open the gate.

## From gate to agora {#gate-to-agora}

Of course I'm simplifying *a lot*. You don't get to configure things that easily on your router. First, chances are your router is not running an open source OS, lest you break/brick it you see. Then there's the actual configuration. Say your run OpenWRT/LEDE. Configuration is not straightforward and even web interfaces don't provide simple configuration steps to achieve what I've presented. A gap certainly needs to be filled to transform the gates into something that brings value and services to the user ; into something that connects the user to others instead of blocking interaction.

Opening the gate to a kind of communication closer to the user's needs and the real collaborative Internet we thought to build is litterally a one step process: get root access. Usually if the router is running an open source software, you already have access with your admin password. Once in possession of the router credentials, sky's the limit… for sysadmins. Of course I don't expect even tech-savvy users to know how to configure properly so many aspects of a router. Instead we should commission experts to do the configuration for us. For instance knowledgeable friends, CHATONS or associative ISPs, etc.

In a world where boxes are ever more tightly bound to services provided by giant ISPs that don't provide Internet access only anymore, we have to adapt and think ahead of them. They already provide VoIP and TVoIP services in triple-play offers. That's of course out of the scope of what an ISP should offer, but that doesn't mean we shouldn't care about them either. It's added value to their customers, and a deterrent to move away from their offers, especially considering their price which is often more competitive. Providing extra features at the router level, we not only catch up with big telcos, we also provide the direct embodiement of what open networks allow.

Plenty of technologies could feel right at home in a router. A networked hard disk connected over USB? It's usually only available to your LAN, but what if you have a configured SFTP access from the outside? What if you have a configured VPN that allows you to access your LAN wherever you are? What if you have a configured binding of two LAN over a VPN? You would never need remote storage again.

There is the need for a tool that allows easy sharing of these credentials. A tool that would allow such remote access with trusted third parties. A tool that would allow delegation of configuration tasks.

## The brick and mortar of the agora {#brick-and-mortar}

Configuration tasks are tedious, even to the experienced user. Hopefully we don't need to remember every single task - that would be error-prone - and can rely on community-contributed scripts such as ansible playbooks/roles to configure tasks and do the necessary checks before changing anything. LXC containers coming to the rescue, we can prevent the added programs from interfering with the essential features of your router.

We need to provide the building blocks for the agora. It's the next stage to build Internet communities and create more interest in both self-hosting and associative ISPs.
