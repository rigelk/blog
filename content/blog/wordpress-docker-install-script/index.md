+++
title = 'Wordpress docker install script'
description = "I swear I don't use Wordpress anymore - it is a very useful playgroand to teach basic web development though."
date = 2016-06-05
[taxonomies]
tags = ["docker", "web"]
categories = ["tutorial"]
+++

A few weeks ago I was looking at wordpress more closely as I was trying to teach a few students at my local university to write wordpress themes. Problem is: **they don’t have a LAMP stack**, nor know what it is, nor need to know what it is at first.
The beginners’ catch

Web servers. Web servers are painful as hell to consistently maintain on machines you don’t know at all. My students had enough various OSes to give me headaches – which is a good thing, i like variety. But when it comes to teaching newcomers how easy it is to setup a CMS, you need a **consistent install indications**.

That’s where Docker comes in handy. One container to rule them all, they say. No dependancies, no hassle. **run the container and open the page**!

Well, I couldn’t tell students to install docker themthelves. So I wrote a [script](https://gist.github.com/rigelk/b244c62fa79f912c2634) for GNU/Linux for starters:

- it installs docker thanks to the [getdocker magical script](http://get.docker.com/)
- it gets the latest wordpress
- it gets dummy content for wordpress
- it gets docker-enter
- it runs a nginx + mysql container
- it enters the container

{{ gist(url="https://gist.github.com/rigelk/b244c62fa79f912c2634") }}

All you have to do is curl it and execute it straightforward. A useful oneliner.