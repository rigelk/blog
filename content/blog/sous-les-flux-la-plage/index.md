+++
title = "Sous les flux, la plage"
description = "Flux d'informations, flux de code, flux d'injonctions à produire des flux sans plage au bout. De la place d'un informatique sous les tropiques à l'ère du flux tendu."
date = 2020-04-28
draft = true
[taxonomies]
tags = ["web"]
categories = ["opinion"]
+++

Quelle est votre rive, votre plage ? Quelle est votre vision d'un futur radieux et de navigateurs qui chantent ? La question ne se résume évidemment pas à votre prochain voyage vers les sables fins, ni à un web sans pub. La question porterait plutôt sur ce grin de sable, profondément logé dans votre cortex. Vous pouvez l'oublier un moment, mais vous ne pouvez mettre le doigt dessus. Aujourd'hui, parlons de ce grin de sable dans nos vie qu'est le numérique.

Contrairement à notre relation avec beaucoup d'autres outils, nous avons du mal à nous satisfaire des technologies qui nous entourent dès lors qu'elles sont informatiques. Quel mot-clef rechercher ? Quel site visiter ? Quelle application installer ? Quelle information retenir dans ce flot incessant qui nous assaille ? Et quand ces questions trouvent réponse, toujours d'autres : comment installer ce programme ; Est-il trop gourmand ; Faut-il le mettre à jour ; Quel impact sur ma vie privée ? Quelle gouvernance pour son dévelopement ? Quelle dynamique sociale favorise-t-il ? En général, une fois arrivé là, on est confus, et l'on se demande si Dante n'avait pas décrit une visioconférence en plein confinement.

Ce que je vous ai décrit, peut-être que vous vous y reconnaissez, mais vous devez certainement le trouver réducteur. Après tout, ne serait-ce pas le parcours normal de tout utilisateur ? L'utilisateur est bien propre maître de ses choix, alors qu'il les fasse, non ? Et puis avec un peu d'habitude, et quelques astuces… Votre ami informaticien doit bien pouvoir vous aider à trouver la bonne information, non ? Mauvaise nouvelle, les informaticiens dont je fais partie sont probablement les plus névrosés de tous, cherchant constamment le programme le plus adapté sous couvert de veille technologique, érigeant le dénigrement des langages qu'ils ne codent pas en sport international. Ces « sachants » sont souvent les premiers à baisser les bras, parce qu'ils ont conscience de l'ampleur de la tache. Mais aussi parce qu'ils sont contraints de subir ces technologies - ils sont payés pour ça.

On dit souvent le savoir libérateur, mais quel savoir pour se libérer des chaînes de notre relation BDSM avec le numérique ? Car oui, admettez au moins aimer ces vidéos recommandées, ces enceintes connectées. Elles viennent se rajouter à la liste de doudous numériques, et si elles ne répondent pas à vos questions, elles viennent se substituer aux réponses. 

Et si on donnait les bonnes réponses ? Autrement dit, si on diffusait un savoir du numérique à tout le monde (appelons-le B2i), que ce passerait-il ? Est-ce que pour autant on pourrait entrevoir un rapport plus pacifié au numérique ?

### Les informaticiens rêvent-ils de voitures autonomes électriques ?

Appelons informaticiens ceux qui sont assez courageux pour répondre aux questions de leur entourage. Assurément, un rêve partagé par tous est de ne plus avoir à ces questions ; non pas qu'ils ne veulent pas aider, mais parce qu'elles leur rappelle le décalage constant du reste de la société avec leur problèmes. En bref, leur rapport à l'informatique n'est pas le même. Si répondre se vit comme une réussite sociale au début, très vite l'euphorie s'estompe pour devenir une mécanique usante.

L'informatique est un lieu de friction, qui comme cette mécanique toute simple des questions de nos proches, vient user notre corde. Mais des frictions, il y en a bien d'autres quand comme moi vous faites ce métier.

D'abord, des frictions avec la machine, contre laquelle on se bat pour lui faire exécuter ce que l'on souhaite. Il faut choisir un langage, apprendre à le dompter, réaliser qu'il y a en fait derrière cette chimère des gens qui lui donnent corps avec autant de mises à jour et de bibliothèques logicielles, pour enfin se rendre compte que c'est une course à la norme sociale - fût-elle codée dans un langage informatique. Il y a des mesures de _bon_ code, des revues de code qui viennent enlever le _mauvais_ code, des tests qui tamisent votre logique, et des déploiements qui la défient. Rêve-t-on pour autant d'un langage qui viendrait minimiser la friction à chaque étape ?

Il y a des des frictions avec les utilisateurs, qui nous rapportent des souhaits et bugs en flux continu. Ils sont le révélateur de l'utilité de notre code et donc notre plus grande source d'espoir, mais le revers de cette médaille est aussi la douleur des rapports qui viennent confirmer notre incompétence ou souligner l'urgence d'une fonctionalité à laquelle nous aurions dû penser plus tôt. Rêve-t-on pour autant de dialogues plus sereins ?

des frictions avec les autres informaticiens, qui ne sont pas d'accord avec notre manière de développer ; des frictions avec nous-mêmes, qui nous battons pour que notre code serve à quelque chose.

Chaque flux d'information et de code est soumis à des attentes et à des frictions qui dépendent de notre rapport personnel au numérique, mais bien plus du rapport de toute la société au numérique - même si vos utilisateurs sont experts. Ne faudrait-il pas aussi et surtout rêver de l'améliorer pour détendre ces flux qui nous assaillent ?

### Le rivage d'en face, le nouveau monde

Vous avez trouvé cette plage, ce rivage. Vous y trempez les orteils, l'écume des vagues lèchant le sable humide qui les entoure. Quelqu'un s'approche de vous - il·le semble éviter l'eau qui vous est pourtant si agréable. Arrivé·e à portée de voix, il lance :

« Vous savez, cette plage est plutôt morne. Il n'y a personne. De l'autre côté de la presqu'île, vous avez la plus grande plage du coin. »

Vous vous demandez vraiment si c'est une bonne idée. Il y a plein de plages de galets après tout.

« Si vous êtes assez Agile, vous y trouverez votre dream team. » continue-t-il.

À ces mots, vous remarquez que votre interlocuteur·i·ce porte ce costume si cher aux entrepreneurs qui auraient plus leur place parmi les sirènes de la startup nation que sur la plage. Ils rôdent sur les dunes qui bordent les côtes, et vont prêcher leur parole aux jeunes baigneurs. Vous décidez de l'ignorer - vous avez choisi cette plage, après tout. D'autres baigneurs ne sont pas aussi déterminés.

---

Cette autre rivage que l'on nous promet - quand il ne s'agit pas d'une rue - c'est un monde de révolutions technologiques, souvent présenté comme portée par la vision géniale d'un·e entrepreneu·r·se. On se demande ce qui les rend si heureux, performants, ou les deux à la fois. On nous présente leur personne comme résultant d'un travail herculéen sur eux-même. On nous présente leur équipe et leur organisation, forcément _lean_, forcément _agile_.

_Faites pareil et vous sauverez la nation !_ devrait lire le présentateur. Pas que l'on ait vraiment besoin de sous-texte.

Comme toute une partie de la population n'est pas assez _entrepreneuse_, trop _fonctionnaire_, une partie - et soyons clairs, une majorité - des informaticiens n'est pas assez startupeuse selon certains. C'est une pression sociale qui se traduit aussi par des incitations à divers niveaux. Vous êtes étudiant ? Lancez votre startup en étudiant, et si cela échoue, allez vous _ressourcer_ dans une startup-house. Vous êtes post-doctorant et vous n'arrivez pas à trouver un poste ? ~~Transformez~~ transférez votre sujet de recherche payé par de l'argent public en profit privé. Votre entreprise vous avait en stage ? Faites d'abord vos preuves en autoentrepreneu·r·se avant de _mériter_ un CDI. 

Bien sûr, cela n'enlève rien aux entrepreneurs qui en fondant leur propre univers, cherchent à répondre à leurs envies et manques. Ils ont peut-être été poussés par un besoin de création impossible ailleurs. Ils ont peut-être été dégoutés par une hiérarchie inique, ou un management de petits chefs. Là n'est pas la question - on parle de plage et de sable fin après tout, non ? Le problème est bien plus sociétal, et dans la vision que l'on se fait de la valeur - en l'occurence, la valeur d'un informaticien comme d'un génie dans une bouteille que seul l'entreprenariat peut ramasser sur sa plage. La plage n'est pas tant menacée par le désir des entrepreneur de la terrasser, mais par l'idée qu'il n'y a que des plages à terrasser.

Pourquoi mettre les entrepreneurs, et a fortiori les informaticiens sur un piedéstal ? Avant d'être un créateur, l'entrepreneur est en rupture avec son environement. Mais a-t-on besoin d'être en rupture pour être créateur ? Il y a bien des chercheurs qui créent dans un environement très codifié de revue par les pairs. Le fait que l'on identifie la valeur d'un individu dans sa rupture avec le société est pour le moins étrange. On n'est pas habitué à entendre les louages des ascètes ou des survivalistes, qui pourtant font une rupture bien réelle - fusse-t-elle temporaire. La rupture de l'entrepreneur est pourtant bien à l'origine des vertus quasi-homéopathiques de la _disruption_ que vantent les acteurs étatiques comme privés qui financent et orientent le secteur professionnel informatique.

L'informatique moderne a toujours été en décalage avec son temps. L'Internet des débuts était vu comme une extension de l'esprit permettant à l'homme de dépasser les clivages sociaux, à la manière des drogues qui connaissaient alors un boom. Quand plus récemment, l'informatique a permis de rompre avec les besoins de personnels en automatisant des taches jusque-là manuelles, cette capacité de rupture a vu sa trajectoire infléchie. Jusque-là pensée pour casser des normes, contourner des lois, et permettre une libération par et pour le cyberespace[^1], l'informatique s'est à nouveau vue colonisée par les entreprises.

### Coloniser les machines, les logiciels, les esprits

Les doux rêves de moutons électriques et de paradis numériques tranchaient déjà beaucoup avec les origines très militaires d'Internet[^2] et aux constructeurs de matériel de l'époque - on se souvient toujours de l'impact du quasi-monopole d'IBM sur les machines de l'époque, faits de logiciels propriétaires (sans code source fournis). Les idéaux se frottent alors aux impératifs capitalistes : le code comme les machines sont vus comme une ressource qu'il faut garder sous contrôle de l'entreprise pour lui assurer des clients capitifs et donc des revenus futurs. Ces pratiques, si elle continuent à nos jours, ont été tout particulièrement critiquées dans les années 80 jusqu'à donner naissance à tout un mouvement dit du « logiciel libre », cherchant à protéger le code créé par des individus pour en assurer la capacité d'étude/modification/amélioration/distribution au fil de ses utilisations et réutilisations.

Véritale contre-attaque à la fermeture du code fourni avec les machines de l'époque, le logiciel libre a toujours été difficilement accepté par les entreprises qui lui voient trop de restrictions et d'obligation de partage. Il lui préfèrent l'« open source », une pratique démocratisée d'usage de licences plus souples pour partager son code. Le code open source ne s'encombre pas de préserver sa nature au fil de ses réutilisations dans d'autres logiciels, et convient donc très bien pour les entreprises, qui peuvent alors se permettre de le réutiliser.

La subtilité réside dans la perception de ces licences par les développeurs-individus, hors des murs de l'entreprise, qui cherchent à meubler leur CV avec du logiciel-vitrine ou simplement qui partagent du code dont ils ont un usage direct. La plupart cherchent à partager du code qu'ils ne vendront de toute façon pas, pensant contribuer à un commun qui peut être librement réutilisé par d'autres développeurs comme eux, sans penser qu'il peut aussi être librement exploité par des entreprises pour contribuer à leur profit.

L'open source est une forme de flux tendu de l'exploitation des développeurs, et sa particularité est d'exploiter un partage qui se fait naturellement, plus encore que pour les métiers du design qui peuvent se permettre de ne partager en ligne que les rendus de leur travail pour constituer leur portfolio. Les développeurs sortant d'école ou d'université n'ont généralement aucune formation aux licences et n'envisagent pas les répercussions que le choix d'une licence permissive ou libre peut avoir. De même, leur cursus comporte souvent des modules d'enseignement à l'entreprenariat sans évoquer les modèles commerciaux existant autour de logiciel libre. C'est toute la perception de générations de développeurs qui est modifiée pour favoriser leur exploitation, et réduire les communs numériques à des standards décidés par les mêmes entreprises qui profitent de cette exploitation.

[^1]: Barlow John Perry, « Déclaration d'indépendance du cyberespace » ([en](https://www.eff.org/cyberspace-independence)) ([fr](https://www.cairn.info/libres-enfants-du-savoir-numerique--9782841620432-page-47.htm%C3%A2%E2%82%AC%C2%A6))

[^2]: Alexandre Serres, Aux sources d’Internet : l’émergence d’ARPANET, thèse de doctorat sciences de l’information et de la Communication : université Rennes-II, 2000. 2 vol., 676 p ([fr](http://tel.archives-ouvertes.fr/tel-00312005/fr/))
