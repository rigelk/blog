+++
title = 'ZeroNet, quest for a decentralised and anonymous internet'
description = "There are only few systems that make practical anonymous services. ZeroNet is one of them, relying on the Tor network and putting tools that make websites running on top of it more efficient and scalable. Here be dragons, this is alpha software."
published = true
date = 2017-08-26
[taxonomies]
tags = ["p2p", "web"]
categories = ["test"]
[extra]
glossary = [
  {word='Websocket', def='WebSocket is an independent TCP-based protocol that can be found in web browsers and web servers, providing full-duplex communication channels over a single TCP connection'},
  {word='POC', def='Proof Of Concept'},
  {word='DHT', def='Distributed Hash Table, a form of infrastructure that can be used to build more complex services, such as anycast, cooperative Web caching, distributed file systems, domain name services, instant messaging, multicast, and also peer-to-peer file sharing and content distribution systems'},
  {word='UDP', def='User Datagram Protocol, one of the core members of the Internet protocol suite.'},
  {word='i2p', def='Invisible Internet Project, a garlic routing overlay network and darknet that allows applications to send messages to each other pseudonymously and securely'},
  {word='BitTorrent', def='a communication protocol for peer-to-peer file sharing ("P2P") which is used to distribute data and electronic files over the Internet'},
  {word='Magnet Link', def='The Magnet URI scheme defines the format of magnet links, a de facto standard for identifying files by their content, via cryptographic hash value rather than by their location'},
  {word='WebRTC', def='Web Real-Time-Communication, was originally developed to aid certain types of connections between browsers without the need for an additional plugin, and uses Javascript to do so'},
]
+++

After testing a bit around Zeronet, I wanted to know how to create a somehow "heavy" site on ZeroNet. That is, a site hosting quite a lot a data (ex: YouTube clone). Exploring for ways to do this forced me to explore some intricacies of ZeroNet.

ZeroNet uses a combination of BitTorrent-like protocol, a custom file server and a web based user interface to create a fully decentralized internet overlay. The best thing of that big POC is that it manages to provide a pretty useable experience!

Users run a ZeroNet node and do their web browsing via the local proxy it provides (i.e.: using a local service port in your web browser of choice). Website addresses are public keys, generated using the same algorithm as used for bitcoin addresses. A request for a website key results in the node looking in the bittorrent network for peers that are seeding the site. Peers are selected and ZeroNet connects to the peer directly to a custom file server that it implements. This is used to download the files required for the site. BitTorrent is only used for selecting peers, not for the site contents. Browsing is done through a standard web browser. The interface uses Websockets to communicate with the local node and receive real time information about site updates.

### Technical limitations

Because the website is displayed in a sandboxed iframe there are some restrictions in what it can do. The most obvious is that only relative URLs work in anchor elements. If you click on an absolute URL it does nothing. The sandboxed iframe has the allow-top-navigation option which means you can link to external pages or other ZeroNet sites if you use the target attribute of the anchor element and set it to \_top. So this will work:

    <a href="http://example.com/" target="_top">click me</a>

But this will not:

    <a href="http://example.com/">click me</a>

Dynamic websites are supported, but requires help using centralized services. The ZeroNet node includes an example of a dynamic website called 'ZeroBoard'. This site allows users to enter a message in a form and it's published to a list of messages which all peering nodes will see. It does this by posting the message to an external web application that the author runs on the standard internet. This web app updates a file inside the sites ZeroNet directory and then signs it. The result is published to all peers and they automatically get the update through the Websocket interface.

Although this works it's unfortunate that it relies on a centralized web application. The ZeroNet author has posted that they are looking at decentralized ways of doing this, [maybe](https://github.com/HelloZeroNet/ZeroNet/issues/65) using [bitmessage](https://bitmessage.org/) or some other system of the sort. Something resembling [peer to peer WebRTC](http://peerjs.com/) would be interesting, although it relies on the problematic WebRTC.

ZeroNet seems to be most similar to i2p or freenet. Compared to these it lacks the anonymity and encryption aspects. But it decentralizes the site content which i2p doesn't. Freenet provides decentralization too but does not allow JavaScript in sites. ZeroNet does allow JavaScript but this has the usual security and tracking concerns. ZeroNet can and should be proxied via Tor (it supports it very well within its interface and recommends using it).

### Relies on trackers

Although they are not hard to replace, ZeroNet relies on trackers to discover peers - not to be mistaken with your usual advertisment trackers. ZeroNet protocol is different from torrent, so standard DHT from libtorrent won't work. Also bittorrent DHT is UDP based, so it will not work on Tor without proxying it to TCP. And it does not support storing Tor hidden service addresses, so you would not be able to create sites on Tor.

Further development is [directed to implement a DHT-based discovery](https://github.com/HelloZeroNet/ZeroNet/issues/57), thus liberating from some pitfalls of a tracker-based infrastructure.

### An example of a heavy site: Play

As an uncensorable, unblockable medium, ZeroNet appeals to illegal content. Well, at least to copyright-infringing content distribution. Distributing some content also implies referencing it. A typical example would be thepiratebay, which has become a target for governments and rightsholders alike. So far, the answer has been to make the site copy-friendly, so that it can be recreated elsewhere quickly. With ZeroNet, it could not only be copied, but would be copied *all the time, by each visitor at least a little*.

Play is such a site, with some limitations inherent to the technology used by ZeroNet. So far, these limitations cap the size of a site and thus limit the number of films referenced as magnet links Play can host, but it basically is a POC referencing magnet links of films found on [rarbg.to](https://rarbg.to/).

![A film entry showing a few magnet links in Play](https://user-images.githubusercontent.com/6329880/29749028-39f1632a-8b23-11e7-8e40-7a4ecb36cb4f.jpg)

As everything in ZeroNet is stored client-side via the network overlay, no backend can be used without centralizing the data (which would make the site vulnerable again). So the cap on the site size hits much more than on a regular website. Indeed, a small 8GB database here become a database stored by users, and spread like a torrent would. It's no problem, except that the whole data has to be signed again cryptographically to be spread, so its makes your data pretty difficult to generate *à la PHP*. Again, more [WIP](https://github.com/HelloZeroNet/ZeroNet/issues/1053).

It also forces you to rewrite your website to either a static version that can be committed to the network, or that uses interaction with users of the network via the frontend APIs provided by ZeroNet, or to use a spare backend that defeats the whole decentralized model. The latter should be avoided, and the second has all the limitations aforementioned. I guess that's where the most friction will be felt in the community, but also where you can leverage the most out of ZeroNet. A future access to .onion sites from the ZeroNet client (since it is already so well integrating with Tor as a proxy) will provide sites the possibility to provide a .onion backend in addition, which will render the latter option more desirable, albeit still centralized.

### Anyone using it?

The base of ZeroNet itself is bundled with a default interface that helps discover the websites you access (who serves it, what kind of data from that site you have downloaded and wish to host), but also explore new ones via portals or the default links already included. Indeed, the ZeroNet projet also created a few POC sites to feature the possibilities.

![ZeroNet's home interface](https://camo.githubusercontent.com/4629a7d44a828f5bb20cedd90522ae62f0947b35/68747470733a2f2f692e696d6775722e636f6d2f4836304f4148592e706e67)

As for these "main sites" provided by the development team, they feature a few hundred core users:

- CMS/blog: `http://127.0.0.1:43110/18zZNJnTry34f681g2Ur7S2gKsYa2LqPTr`
- social network: `http://127.0.0.1:43110/Me.ZeroNetwork.bit/`
- YouTube: `http://127.0.0.1:43110/1FUQPLXHimgCvYHH7v3bJXspJ7bMBUXcEb/`
- OpenStreetMap mirror: `http://127.0.0.1:43110/1yUji4qPD7GAog8XkP3EDMEfrL995S4Xd/`
- reddit alternative: `http://127.0.0.1:43110/Board.ZeroNetwork.bit/`
- forum: `http://127.0.0.1:43110/Talk.ZeroNetwork.bit/`
- search engine: `http://127.0.0.1:43110/kaffiene.bit/`
- email: `http://127.0.0.1:43110/1MaiL5gfBM1cyb4a8e3iiL8L5gXmoAJu27`

The Github issues and PR feature a few hundred interactions as well, so you might not say they are just gonna stay at the alpha level, but they sure need so more love to push forward ideas and fix current infrastructure limitations.

Also, bear in mind ZeroNet is *not fully audited* and sees much less eyes on each commit that Tor does, for instance. So don't put ultra-sensitive data there. It *will* eat your cat and burn your house.

I'm currently seeing the way sites are shared as quite futuristic (at least as a technology mix) and potentially very interesting if your goal is to spread information to *a lot of people* (see my [draft about relieving alternatives to the cloud](@/blog/draft-notes-1/index.md)).
