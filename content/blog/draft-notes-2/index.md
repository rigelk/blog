+++
title = 'Draft notes #2'
description = "Compressing assets with Grav, and being fed up (temporarily) about the state of JavaScript."
date = 2017-08-28
related_pages = [
  'draft-notes-1'
]
[taxonomies]
tags = ["programming", "web"]
categories = ["test"]
+++

This week's focus was mainly on getting the blog to comply with PageSpeed Insights's recommandation, and globally getting a finer understanding on best practices for serving this blog to you with my small VPS.

It so appears that *compressing assets is of utmost importance*, and while I already knew that through my previous experiences using gulp and other JS-based compilation tools, I also assumed nothing similar existed in the PHP world. 
I have been using a Ghost blog previously, and making both Ghost and Wordpress themes for my previous blogs using npm tools. I've also done some SPAs for my work, which of course were sites running entirely on the client side with little to no server interaction. That plus the whole trend of using JS everywhere lead me to think you couldn't do it in two clicks in PHP.

Oh boy I was wrong.

### Compressing assets

Shrinking, compressing assets is what pipelines are for. Think of it as a meat grinder for Javascript, CSS files, and even more. They help solve optimization problems in website delivery:

- removing blanks, using shorter variable names, etc… compressing what can be compressed make your assets load faster. That's the naive solution to speed up your website.
- merging files when it makes sense, so that the client doesn't have to spawn a new HTTP connection (or new stream in HTTP/2, even if that's less costly) for each file but one.
- versioning files to bypass cache when a new set of files are generated.

But often, these are done in JS, or are left aside, like that of async loading of assets in the right order for optimal page load.

#### Grav's fabulously simple Asset Manager

It does everything at once! Thanks to its Twig support, it's as easy as putting the following snippet in your your Grav site:

```twig
  {% block javascripts %}
    {% do assets.addJs('jquery',{'priority':110,'pipeline':true}) %}
    {% do assets.addJs('theme://js/kube.min.js',{'pipeline':true,'group':'head-async'}) %}
  {% endblock %}

  {{ assets.js('head-async',{'loading':'async'}) }}

  {% block stylesheets %}
    {% do assets.addCss('theme://css/kube.min.css') %}
    {% do assets.addCss('theme://css/custom.min.css') %}
  {% endblock %}
  {{ assets.css() }}
  
  ... <!--- after the footer --->
  
  {{ assets.js('head',{'loading':'sync'}) }}
```

And at the end you get two ordered asset files (picture depicts a different version with synchronized JS assets):

![](2017-08-28-034459_796x155_scrot.png)

### But why not that shiny Javascript?

Grav - the blogging system I use - being a PHP-based CMS, processes of course everything server-side. A Javascript-based CMS would probably put the load of computing on the client. While it can be useful if you *need* an ultra-scalable application with thousand of clients for a minimal server load, bear in mind that most of the time:

- usually, that means more files to send to the client (unpractical in unreliable/+slow mobile network settings)
- at best, that means the device will have to compute more (slowing even more light devices as phones)
- at worst, that means more bugs, as Javascript is a cursed language.

#### Vade retro, ECMAScript

![Book size for JS](https://cdn-images-1.medium.com/max/800/1*Kd0UGiDvgooFooCy28rs8Q.jpeg)

Oh yeah, I now try to limit my use of Javascript. Why? Let's recap with [something I read on Medium](https://hackernoon.com/the-javascript-phenomenon-is-a-mass-psychosis-57adebb09359):

> I ran a software business for a couple of years. It got bought and right now Im developing the very product I used to sell. My former employees put AngularJs and Node.js in there. I remember my conversation 3 years ago with my best engineer : he said that javascript was taking over everything. I thought “Wow. They managed to fix that horrible language”
>
> Well, no. And it’s worse, because at least before, we were screwing up small things with JS, it was a toy. The thing is, there is a mass psychosis about JS and it’s like everybody is pretending that it isn’t awful. And then, as if this wasn’t bad enough, someone had the brilliant idea of putting this thing in the backend. Nodejs is costing millions per year to naive companies who are adopting it. You were wondering who they are: they are startups and small companies.
>
> Mind you, these engineers are smart, but they’re weak against crowd thinking.
>
> At my new company, everyone was pretending that JS was alright. I got tired and spoke up. Turns out, deep down they all hated JS, it was just crowd thinking. Now they all hate JS. And we’re waiting impatiently for Web Assembly.

One of the most amazing and distressing things about JavaScript is that it can actually fail silently at runtime due to syntactical errors! Another thing is “callback hell” which promises can mitigate but are otherwise not a perfect solution. The most notorious of JavaScript’s faults is probably in its weak typing (not to be confused with dynamic typing) which manifests in the profusion of [WATs and WTFs](https://www.youtube.com/watch?v=2pL28CcEijU) that make JavaScript the butt of so many industry jokes.

{{ youtube(id="2pL28CcEijU") }}

The language is so bad that the use of a linter (such as JSLint or ESLint) is practically mandated for all JavaScript programmers. This, despite the fact that ECMAScript has undergone many, many improvements in recent years culminating in ES6. Apparently, the ECMA TC39 committee is unable to completely eliminate all of JavaScript’s most egregious faults.

The thing is, it's broken, but it's been bundled first by Netscape and forced unto us when IE was popular and ever since, grew into a large user community and ecosystem of tools and libraries, not to mention the large number of jobs available for JavaScript developers. This answer also explains the longevity of all the mainstream languages, irrespective of their faults, including: PHP, C++, Perl, and Visual Basic (languages everybody loves to hate).

It just has too many faults and a litteral monopoly on browser runtime to be left excused with that.

### Use Javascript, just not the way you learned it

You can use languages that transpile to JavaScript to avoid its pitfalls or bugs. [Here](https://medium.com/javascript-non-grata/the-super-surrogates-of-javascript-862460199915) are some of the better ones, but [there are many, many languages to choose from](https://github.com/jashkenas/coffeescript/wiki/list-of-languages-that-compile-to-js). For front-end development, you don’t have to choose JavaScript unless you’re sheep.

So it’s really your choice. It’s up to you whether you want to dive into the chaotic world of JavaScript. That’s where the money is in terms of front-end jobs. But that’s also where the unholy mess of JS web frameworks is, which leads to “framework fatigue.” Angular 1, Angular 2, React, Ember, Meteor, Backbone, Knockout, Mercury, Polymer, Aurelia, Mithril, Vue, etc. I've even personally used Angular 1/2, Ember, Meteor and React (my favorite). I would even have used Knockout if my projects were not to be used by others! But I definitely felt that fatigue.

While I still haven't chosen a transpiler to start with, I have come accross [one I like in its own setting](http://witheve.com/philosophy/).

### A bright light in the future: WebAssembly

- *An improvement to JavaScript*: Implement your performance critical stuff in wasm and import it like a standard JavaScript module.
- *A new language*: WebAssembly code defines an AST (Abstract Syntax Tree) represented in a binary format. You can author and debug in a text format so it’s readable.
- *A browser improvement*: Browsers will understand the binary format, which means we’ll be able to compile binary bundles that compress smaller than the text JavaScript we use today. Smaller payloads mean faster delivery. Depending on compile-time optimization opportunities, WebAssembly bundles may run faster than JavaScript, too!
- *A Compile Target*: A way for other languages to get first-class binary support across the entire web platform stack.

Oh my, I diverged again from the initial subject, right?
