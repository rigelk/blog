+++
title = 'Good reads!'
description = "I wish I had more time to read books. Maybe you can read some of my collection though, so let's review it!"
date = 2016-06-24
[taxonomies]
tags = ["books"]
categories = ["opinion"]
+++

Who would ever think of keeping track of books read and of the eventual progress in their reading? Well, that’s part of the concept of [goodreads.com](http://goodreads.com/), or at least the way I make use of its features since I discovered it a few days ago.

A quite small change in my life, but a useful one. Keeping track of activities has always proven useful (GitHub, Redmine, or for some hardcore users, social networks themselves), but I would never have expected that for books.

The site itself encourages you to register the books you already read, want to read or are currently reading. It acts like a to-to list, and for me as a way to showcase my readings (be it along my CV or to my friends to get recommendations based on what I’ve read). That creates a motivating synergy which made me read some books again and post update statuses of those I was reading.

Plus, I often read technical books and it is quite hard to find varied comments about them. Recently I found myself reading Introduction to Algorithms (Cormen, editions Dunod) and found only (extremely) positive comments on developer sites. As with masses, comments tend to reach a point where all counter-opinion is either ignored or not valued enough. Here on Goodreads, comments were surprisingly varied (though still positive) and easily [pointed to alternatives](https://www.goodreads.com/review/show/583842291).

If only traditional social networks were as refreshing!

P.S: a link with my [goodreads profile](http://goodreads.com/rigelk), would you want to recommend me something.