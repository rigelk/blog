+++
title = 'Carnet de dévelopment sur PeerTube, étape 2.2'
description = "Vous n'avez pas suivi le dévelopment de PeerTube ces 3 derniers mois ? En attendant sa sortie, retour sur quelques fonctionalités développées par la communauté que j'ai dans le viseur."
date = 2020-05-29
[taxonomies]
tags = ["web", "peertube", "programming"]
categories = ["tutorial"]
[extra]
mastodon = "https://miaou.drycat.fr/@rigelk/104252718627964987"
+++

La 2.0 était tout juste passée que j'ai remis le pied à l'étrier du dévelopment de PeerTube. J'avais alors enfin libéré un peu de temps libre, et décidé de m'attaquer à un gros morceau : la page d'une vidéo, a.k.a. la _watch page_. Vous la connaissez, c'est la page de PeerTube avec le lecteur vidéo et le fil de commentaires. Au menu, boutons plus discrets, subtils petit changements de position des commentaires et de la présentation de leurs auteurs, entre autres choses.

À ce moment, j'avais en tête plusieurs améliorations de l'interface utilisateur, le principal souci selon moi à l'époque (et qui le restera à mes yeux vu l'habitude qu'ont les utilisateurs pour les interfaces material/flat à souhait, mais c'est un autre sujet…). En quelques mois, le design a déjà beaucoup changé - mais surtout mes compétences en CSS… - tout en gardant les fondamentaux. Est-ce que PeerTube est toujours *moche* ? Certainement. Surtout si on le compare à ses concurrents GAFAM. Mais bon, un octet à la fois, comme on dit, et c'est déjà bien assez. On tag la 2.1, on release, et on recommence à charbonner.

Il faut dire que le design d'interface, c'est pas notre truc, même si personnellement j'adore ça. On se forme un peu sur le tas, et forcément, ça prend un peu plus de temps (quelque années…) que prévu. Il y a encore beaucoup à faire, et on avance toujours à l'aveugle. Mais voyez-vous, j'ai l'impression d'avoir passé trop de temps sur l'UI visible de tous quand il reste tant à faire côté pile : les menus d'administration.

C'est la deuxième chose qui m'a occupé ces derniers mois : passer en revue les menus d'administration, et essayer de les "rafraîchir" sans changer tout non plus, vu que juin devrait voir le premier contact du projet PeerTube avec de vrais designers[^1] pour critiquer et repenser l'administration.

[^1]: le design initial de PeerTube, assuré par Olivier Massain, n'avait réfléchi qu'à la partie visible à tous de l'UI. Toute la partie administration, paramétrage, modération multi-niveaux, etc. n'était pas imaginée.

### Passer du beau à l'informatif

Ma transition aux menus d'administration ne s'est pas faite sans heurts. Ces menus m'étaient pour la plupart peu familiers, étant un développeur de PeerTube sans faire moi-même beaucoup de tâches d'administration d'instance : il y a en effet peu de contenus à modérer sur les serveurs de Framasoft. Sauf que de serveur d'hébergement à petite échelle, PeerTube est de plus en plus utilisé pour héberger de multiples utilisateurs, parfois inconnus des administrateurs ! Et là, les menus d'administration sont forcément austères.

Mais plus que de beaux menus, les administrateurs ont besoin de menus qui intègrent des explications. On ne naît jamais administrateur d'instance, voyez-vous, et la documentation prend du temps à assimiler sans forcément décrire mot à mot toutes les options.

{{ image(path="menus_administration", webp=true, png=true, caption="Menu d'administration, avec des descriptions extensives de section et de fonctionalités", style="width: 90%") }}

Un autre souci qui est couramment évoqué - ou du moins que les issues nous font remarquer, est celui des fonctionalités activées à la hâte par les administrateurs, quand bien même leur impact est important. Par exemple, le fait d'activer les inscriptions sans mise en quarantaine des nouveaux utilisateurs, ou encore le fait de suivre automatiquement un catalogue d'instances sans une équipe de modération suffisante et formée.

Là encore, la direction que nous prenons est celle d'une meilleure information des administrateurs vis-à-vis de fonctionalités qui trouvent leur utilité pour certains, mais causent des effets de bords pas toujours bien compris.

### Rendre les clés

Beaucoup de problèmes inhérents à un monde fédéré relèvent de la _diffusion_ de contenu, mais peu s'occupent de la _découverte_ de contenu. C'est un problème d'autant plus criant pour du contenu à la durée de vie longue comme c'est le cas de PeerTube (par opposition à des systèmes de microblogging qui n'ont la plupart du temps pas besoin d'afficher _tout_ le contenu, seulement le plus récent). Pour résoudre ce problème, il y a plusieurs approches, toutes imparfaites, mais qui sont essentiellement analogues à l'indexation de pages web.

De fait, créer un moteur de recherche est compliqué, et encore plus quand on veut donner aux administrateurs le contrôle de leur moteur de recherche. Actuellement, les instances indexent le contenu local et suivi, mais que ce soit pour découvrir des instances ou découvrir des vidéos en dehors de cet index local, il faut ruser.

Notre technique - héberger une liste d'instances à suivre pour enrichir cet index - est certes basique, mais elle a plusieurs avantages :
- facile à héberger
- facile à comprendre
- facile à modifier

{{ image(path="demenagement", webp=true, png=true, caption="Ça ne va pas sur l'autoroute de la décentralisation, mais ça fait le taf et c'est pas cher", style="width:70%") }}

Jusque-là, elle consistait en un logiciel à autohéberger : celui qui tourne derrière [instances.joinpeertube.org](https://instances.joinpeertube.org/)[^2]. Mais après un an sans voir d'autre installation de ce logiciel, nous avons décidé de permettre l'ajout[^3] du support de fichiers simples listant des instances à suivre. Il ne vous reste plus qu'à mettre une liste d'instances dans un format simple, de l'héberger sur un snippet, gist, paste, ou autre lieu de partage informel, voire de l'éditer collaborativement via un système de fusion tel qu'on en trouve sur les forges logicielles.

Mais à quoi ressemble une telle liste, au juste ?

{{ gist(url="https://gist.github.com/rigelk/0da4ce7ff821e0a16728bc397e600465") }}

Simple comme bonjour :) Et il suffit alors de coller le lien vers le fichier brut en lieu et place d'index pour suivre ces instances dans les 24h, et rester à jour en cas de modification de ce fichier. Utile pour de la curation collaborative ! J'attends avec impatience la création de petites listes recommandant des instances ! Et qui sait, vous me verrez peut-être en relayer.

[^2]: [https://framagit.org/framasoft/peertube/instances-peertube](https://framagit.org/framasoft/peertube/instances-peertube)

[^3]: cette fonctionalité existait déjà, mais le fichier devait impérativement être servi sur la route `/api/v1/instances/hosts`, ne permettant qu'à des administrateurs rompus aux alias de reverse-proxy de servir une telle liste

### Encore plus de responsabilités

Vous voyez, rendre les clés de la voiture (la liste d'instances dans mon analogie, suivez un peu!) n'est pas assez, il faut aussi que l'on vous rende les clés de tout ce qui est à venir dans PeerTube. Car oui, si on parle de décentralisation, ce n'est pas toujours parfait, ça se repose sur des points de défaillance unique (des serveurs centraux) pour l'exemple, mais jamais _un_ serveur central dont on ne vous donnerait  pas les clés. Il faut toujours que vous puissiez l'héberger vous-même.

Le site de [joinpeertube.org](https://joinpeertube.org) ? Le code source est [là](https://framagit.org/framasoft/peertube/joinpeertube/).

Le site de [la liste d'instances](https://instances.joinpeertube.org/) ? Le code source est [là](https://framagit.org/framasoft/peertube/instances-peertube).

Et naturellement, si on propose un autre service par facilité, ce n'est pas pour vous l'imposer, c'est pour faciliter son essai pour que vous puissiez mieux choisir par la suite de l'héberger vous-même, ou via un [CHATONS](https://chatons.org/) ou structure similaire. En somme, c'est la même chose que ce que Framasoft fait vis-à-vis de tout un tas de logiciels, qu'elle met à disposition tout en invitant ceux qui le peuvent à faire leur part.

#### Les clés du camion de la recherche globale

Certains auront remarqué l'annonce d'une recherche globale. Elle permet de trouver via la barre de recherche d'une instance PeerTube ayant activé cette fonctionalité des vidéos qui n'appartiennent pas qu'aux instances suivies par cette instance, mais de toute instance connue d'un index faisant tourner ce futur moteur de recherche globale. C'est notre seconde réponse au problème (qui encore une fois n'est pas un problème pour tout le monde, PeerTube pouvant très bien être satisfaisant en n'indexant que quelques communautés d'intérêts) évoqué plus tôt, celui de la découverte de contenu.

{{ image(path="search", webp=true, png=true, caption="Mockup préliminaire de recherche globale, qui a justifié l'ajout d'un typeahead dans la version 2.1", style="width: 60%") }}

Et comme pour la voiture, on vous rendra les clés du camion ! Sauf que cette fois, l'index ne tiendra pas sur une simple liste textuelle, j'en conviens. Il faudra évidemment un déploiement un peu plus conséquent pour indexer chaque jour les nouvelles vidéos d'un ensemble d'instances, dont on a vu qu'il compte désormais plusieurs centaines de milliers de vidéos. C'est une des raisons (que je détaille plus bas) pour laquelles nous ne l'avons pas intégré de facto à chaque instance.

> l'index d'instances et l'index de recherche gloable affectant grandement votre politique éditoriale, il est crucial que les communautés s'emparent et hébergent ces outils

Ce sera probablement une fonctionalité dont beaucoup ne voudront pas, car elle permet aux utilisateurs de voir au-delà des choix éditoriaux de leur administrateur d'instance, les choix éditoriaux de l'administrateur de la liste suivie par l'indexeur (le fameux camion). Ce sera activable par l'administrateur de l'instance, mais le mieux reste encore que quelques administrateurs d'instances pensent aussi à héberger ce camion pour leur communauté. En fait, nous vous incitons fortement à le faire, car les index se servent des listes préalablement établies pour récolter la liste de vidéos qu'ils doivent indexer - l'index d'instances et l'index de recherche gloable affectant grandement votre politique éditoriale, il est crucial que les communautés s'emparent et hébergent ces outils.

Notre proposition se basant d'ElasticSearch, elle ressemble à [https://github.com/silicium14/peertube_index](https://github.com/silicium14/peertube_index) ou [https://framagit.org/rigelk/pndex](https://framagit.org/rigelk/pndex), en faisant en sorte de faire au plus proche du stack technologique existant dans PeerTube pour faciliter sa maintenance.
Et pour ceux que notre implémentation ne satisfait pas, sachez que le réimplémenter est _relativement_ simple. Libre à vous d'utiliser YaCy avec votre propre adapteur d'API entre PeerTube et lui. Car oui, le seul prérequis est qu'un tel indexeur respecte l'[API de recherche documentée](https://docs.joinpeertube.org/api-rest-reference.html#tag/Search) de PeerTube.

#### Petite rétrospective sur la fonctionalité de recherche globale

Après tout ce charabia technique n'ayant pour but que de vous dire d'héberger votre propre indexeur, vous pourriez être intéressés par un peu de contexte quant à l'origine du besoin d'une recherche globale par certains. Dans un passé fort lointain, voici les quelques discussions qui me viennent à l'esprit :

- [https://framacolibri.org/t/ma-video-nest-pas-trouvable-dans-toutes-les-instances/3407](https://framacolibri.org/t/ma-video-nest-pas-trouvable-dans-toutes-les-instances/3407)
- [https://github.com/Chocobozzz/PeerTube/issues/824](https://github.com/Chocobozzz/PeerTube/issues/824)

Naturellement, en plus de ces discussions, il y a eu de nombreux retours qui font état d'une attente de trouver _tout_ le contenu du réseaux PeerTube sur n'importe quelle instance. C'est impossible à satisfaire, bien entendu, mais certains se satisferont d'un entre-deux. C'est à ces personnes que la recherche globale s'adresse - et en aucun cas la recherche globale sous cette forme ne prétend être _la_ réponse au problème de la rercherche de contenu sur PeerTube.

Le choix de recourir à un indexeur externe sous la forme d'un serveur central est certainement étrange, surtout quand on cherche à décentraliser. Cette approche en apparence antinomique du but affiché de décentraliser le streaming vidéo vise pourtant bien à faciliter - ou du moins éviter de rendre plus complexe - le déploiement d'instances PeerTube sur des machines peu puissantes, et donc leur décentralisation : on n'a pas perdu de vue le but de pouvoir installer PeerTube sur peu de ressources, même si l'obligation de transcoder localement les vidéos rend cette promesse bancale pour l'instant.

Forcer l'installation d'outils d'indexation sur chaque instance[^4] réduirait grandement la maintenabilité et, pardonnez du néologisme, l'_installabilité_ de PeerTube. On ne veut pas vous faire de fausses promesses sur de l'installation en un clic[^5], mais on ne veut pas non plus mettre des batons dans vos roues. L'installation via YunoHost est probablement le mieux que l'on puisse faire en terme d'installation rapide en production si vous n'avez pas les compétences pour faire un déploiement de production manuel ou via `docker-compose`.

[^4]: plusieurs dizaines/centaines de millions de vidéos indexées ne se font malheureusement pas aussi facilement sur une base de donnée PostgreSQL… Les deux projets d'indexation cités plus haut nous ont permis de faire des comparaisons de performance en vitesse de réponse, et malheureusement, à moins d'avoir un foudre de guerre, PostgreSQL n'est pas assez rapide (à 2M de vidéos, de l'ordre de 2s/req pour une charge de 20 clients, loin du compte) sans passer plus de temps à l'optimiser - du temps que l'on a pas vraiment

[^5]: un déploiement en un clic par exemple via Heroku depuis le README est vu par certains comme un argument de vente, allant jusqu'à intégrer [ngrok](https://ngrok.com/) dans leur déploiement ; cela ne vous donne pas un environement adéquat, d'autant plus pour un service fédéré qui requiert un domaine stable

### De nouvelles clés de modérations

Au cas où je n'aurais pas encore abusé de mon analogie sur les clés, il reste encore plein de choses à faire du côté de l'interface d'administration - et plus particulièrement sur l'interface de modération. Si je vous en parle, c'est évidemment parce que l'on va s'y consacrer dans les mois à venir, mais aussi parce que c'est un _morceau d'UX_ que je n'avais encore jamais abordé dans d'autres projets. De fait, les questions soulevées me sont nouvelles, et passionnantes. Par exemple :

- comment gère-t-on son temps de modération ?
- quelles sont les catégories principales de rapports ?
- quelles actions sont faites le plus régulièrement pour donner suite à un rapport ?
- comment communique l'équipe de modération
- comment se transmet-elle le savoir acquis via les précédents rapports ?

Et déjà, vous pouvez voir que l'on va allier de profondes réflexions d'UX dans la feuille de route 2020, [que l'on a déjà engagé](https://github.com/Chocobozzz/PeerTube/projects/5?card_filter_query=label%3A%22component%3A+moderation+%3Agodmode%3A%22). Liste ultra-non-exhaustive :

- des emails faisant suite aux rapports qui contiennent les détails du rapport et des liens vers des actions
- des actions de masse pour le blocage de domaines, la suppression de commentaires, etc.
- des statistiques de modération par utilisateur, pointant directement vers leurs rapports
- du filtrage par défaut sur les chaînes correspondant aux blocages du compte publiant la vidéo
- des informations et statistiques sur la vidéo et les comptes rapporteur/rapporté accessibles depuis le table listant les rapports
- des morceaux d'API supplémentaires pour les plugins, dont certains ouvrent au filtrage de vidéo à l'upload

Cela nous a mené, grâce aux retours d'administrateurs d'instances, à nous concentrer sur la notion de signalement, déjà existante mais peu ergonomique. Son rôle est central dans la mémoire d'une équipe de modération, et son suivi permet d'éclairer des décisions ultérieures en lien avec les instances ou chaînes impliquées. Il nous a paru assez naturel de rajouter ces informations dans le détail (en cliquant sur la ligne de la table listant les signalements) d'un signalement :

{{ image(path="moderation", webp=true, png=true, caption="Chaque rapport a une vue étendue inspirée de Mastodon, à l'accès rapide et qui contiendra bientôt l'historique d'un rapport") }}

Ces informations sont amenées à être complétées d'un fil de discussion entre modérateurs, ainsi que d'un historique des actions menées (pour, par exemple, retracer les actions sur une vidéo signalée effectuées depuis une autre partie de l'interface, ou inversement, créer un rapport lors d'une modération directe pour en garder trace via un _log de modération_). La même démarche de résumé des informations clés nous a amené à rajouter un panneau d'information à propos d'un utilisateur :

{{ image(path="user", webp=true, png=true, caption="Statistiques de modération d'un utilisateur, où chaque panneau mènera à terme à la liste de contenu concerné", style="width: 60%") }}

Ces fonctionalités sont amenées à être largement complétées, et leur interface raffinée voire repensée, mais l'idée est là. C'est probablement la partie qui m'a le plus questionné - on imagine rarement les expériences utilisateur côté administrateur, et elles sont passionnantes. Vous avez envie de vous y frotter, vous êtes meilleur que moi en CSS (facile!) ? Votre aide est encore et toujours la bienvenue sur la forge de PeerTube :)
