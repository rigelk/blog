+++
title = 'Projects'
template = 'projects.html'
[extra]
no_header = true
+++

## Open source

- <i class="fa fa-peertube" aria-hidden="true"></i> [PeerTube](https://joinpeertube.org) (maintainer), a federated video streaming platform with automatic mirroring, transcoding and live streaming
- [OLKi](https://framagit.org/synalp/olki/olki/) (maintainer), a federated science sharing platform

### Minor projects

- <i class="fa fa-activitypub" aria-hidden="true"></i> [ActivityPub Components](https://w3c-socialcg-aptf.gitlab.io/apcomponents/) (maintainer), web components that can be integrated anywhere
- a bunch of PeerTube plugins I maintain:
  - <i class="fa fa-peertube" aria-hidden="true"></i> [glavlit](https://framagit.org/rigelk/peertube-plugin-glavlit), enhanced moderation tools
  - <i class="fa fa-peertube" aria-hidden="true"></i> [koukoku](https://framagit.org/rigelk/peertube-plugin-koukoku), advertisements with any VAST-compliant server, mindful of DNT.
  - <i class="fa fa-peertube" aria-hidden="true"></i> [slides](https://framagit.org/rigelk/peertube-plugin-slides), adding conference-style slides aside a video
  - <i class="fa fa-peertube" aria-hidden="true"></i> [ungamify](https://framagit.org/rigelk/peertube-plugin-ungamify), removes potential gamifications
  - <i class="fa fa-peertube" aria-hidden="true"></i> [mtcaptcha](https://framagit.org/rigelk/peertube-plugin-mtcaptcha), a captcha service
  - <i class="fa fa-peertube" aria-hidden="true"></i> [hcaptcha](https://framagit.org/rigelk/peertube-plugin-hcaptcha), a captcha service
  - <i class="fa fa-peertube" aria-hidden="true"></i> [recpatcha](https://framagit.org/rigelk/peertube-plugin-recaptcha), a captcha service

### Archived projects

- <i class="fa fa-hubzilla" aria-hidden="true"></i> [Ambroisie](https://framagit.org/rigelk/ambroisie), a WebTorrent port to Elixir using OTP
- <i class="fa fa-peertube" aria-hidden="true"></i> [pndex](https://framagit.org/rigelk/pndex), a PeerTube index built on Hasura/PostgreSQL and Express microservices
- <i class="fa fa-youtube-play" aria-hidden="true"></i> [chatube](https://framagit.org/rigelk/chatube), an Invidious load-balancer for the CHATONS collective which can be adapted for other projects
- <i class="fa fa-gnupg" aria-hidden="true"></i> [gka-libparc](https://framagit.org/rigelk/gka-libparc), a TGDH implementation in OpenSSL, extending libparc to support group key agreement
- [ripmc](https://framagit.org/rigelk/ripmc), an image scrapper that reassembles obfuscated images


## Publications

### Journals

- (writing) Semantic interactions for data repostiories with ActivityPub, co-authored with Christophe Cerisara and Pierre Willaime, LORIA, Nancy.

### Conferences

- (january 2019) [Group Key Agreement in Information-Centric Networks with Tree Group Diffie-Hellman](https://arxiv.org/abs/2004.09966) co-authored with Luigi Iannone, submitted to CoRes 2019 with [code](https://framagit.org/rigelk/gka-libparc), Télécom ParisTech. __arxiv:2004.09966__

## Academic reports

- (september 2018) [Master thesis - survey of Group Key Agreement algorithms](https://drive.google.com/open?id=1efqJEgFZV0ypazu_xSwOlPDQdR_RRBO6) under supervision from Dario Rossi, Polytechnique \& Télécom ParisTech.
- (june 2017) [Experimenting with Skype Quality of Experience](https://drive.google.com/open?id=0B85JC3jpxoTaNGZVYTZYc3BhZXM), co-authored with Olha Prodan at the University of Nice and INRIA.
- (february 2016) [SCADA systems and a retro-engireering of the S7Comm protocol (de)](https://drive.google.com/file/d/0B85JC3jpxoTabTF3QjAyWDB5bTg/view?usp=sharing), during a lab at the FU Berlin.

## Consulting reports

- (june 2015) [Data erasure protocol](https://drive.google.com/file/d/0B85JC3jpxoTaN2tuWXl0aGdnc1E/view?usp=sharing), mandated by Bolloré Protection S.A. to enforce a proper erasure procedure of their aging devices.
